set nocompatible
source $VIMRUNTIME/defaults.vim

augroup vimrcEx
  au!

  autocmd FileType text setlocal textwidth=80
augroup END

if has('syntax') && has('eval')
  packadd! matchit
endif

set mouse=
set tabstop=4
set shiftwidth=4
set linebreak
" set hlsearch
set incsearch
set softtabstop=4
filetype plugin indent on
set autoindent
set noswapfile
autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE
set background=dark
colorscheme material
set termguicolors

"map <MiddleMouse> <Nop>
"map! <MiddleMouse> <Nop>
"map <2-MiddleMouse> <Nop>
"map! <2-MiddleMouse> <Nop>
"
function! OberonSettings()
    set filetype=oberon
    set softtabstop=2
    set tabstop=2
    set shiftwidth=2
endfun"

au BufEnter,BufNew *.mod call OberonSettings()
au BufEnter,BufNew *.Mod call OberonSettings()

let g:c_no_curly_error=1
let g:loaded_matchparen=1
hi Error NONE
hi ErrorMsg NONE
set shm+=I
