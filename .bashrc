# if not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s checkwinsize
shopt -s histappend

alias ls='ls --color=auto'
alias l="ls"
alias mk="make"
alias c="xclip"
alias cs="xclip -selection clipboard"
alias v="xclip -o" 
alias vs="xclip -o -selection clipboard" 
alias grep="grep --color='auto'"
alias p3="python3"
alias kdoc="~/source/script/kdoc"
alias i="~/source/script/backlight.sh +"
alias d="~/source/script/backlight.sh -"
alias sinc="~/source/script/sound.sh +"
alias s="~/source/script/pronounce.sh"
alias stoggle="amixer -D pulse set Master toggle"
alias sdec="~/source/script/sound.sh -"
alias e="exit"
alias info="info --vi-keys"
alias feh="feh -Y --zoom fit --image-bg=#1A1A1A"
alias bluh="bluetoothctl connect 0C:AE:BD:C4:C3:F0"
alias blut="bluetoothctl connect 70:3E:97:57:3B:F6"
alias wgetno="wget --no-check-certificate"
alias midi="timidity -iA -Os"
alias ru="~/source/script/ru"
alias en="~/source/script/back"
alias itch="/home/sevan/.itch/itch"
alias dark="alacritty --config-file ~/.alacritty-alt.toml"
alias gedit="GTK_THEME=Adwaita:dark gedit"
alias li="xrandr --output eDP --brightness $1"
alias gtags="gotags -R ./* > tags"

alias dimv="imv $1 & imv $2"

export PATH="/home/sevan/source/google-cloud-cli/google-cloud-cli/bin:$PATH"

function hyconv() {
    pdftotext $1 $2
}

pdfcompress ()
{
   gs -q -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -dPDFSETTINGS=/screen -dEmbedAllFonts=true -dSubsetFonts=true -dColorImageDownsampleType=/Bicubic -dColorImageResolution=144 -dGrayImageDownsampleType=/Bicubic -dGrayImageResolution=144 -dMonoImageDownsampleType=/Bicubic -dMonoImageResolution=144 -sOutputFile=$1.compressed.pdf $1;
}

alias lpkg='expac "%n %m" -l'\n' -Q $(pacman -Qq) | sort -rhk 2 | less'
alias uxkey='killall -HUP xbindkeys'

COLOR="\[$(tput setaf 6)\]"
RESET="\[$(tput sgr0)\]"
# PS1="${COLOR}\u${RESET} \W :: "
PS1="${COLOR}\W${RESET} :: "

export GOPROXY=direct
export GOSUMDB=off
export GOTELEMETRY=off
export GOTOOLCHAIN=local

export DOTNET_CLI_TELEMETRY_OPTOUT=1

#if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
#  exec tmux
#fi

export VISUAL=vim;
export EDITOR=vim;
export PATH="/opt/voc/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/go/bin:$PATH"
export PATH="/home/sevan/.ghcup/ghc/9.10.1/bin:$PATH"
export PATH="~/.dotnet/tool:$PATH"
export PATH="~/source:$PATH"
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

[ -f "/home/sevan/.ghcup/env" ] && source "/home/sevan/.ghcup/env" # ghcup-env

PLAN9=/home/sevan/source/plan9 export PLAN9
PATH=$PATH:$PLAN9/bin export PATH

take () {
  mkdir "$1"
  cd "$1"
}


. "$HOME/.cargo/env"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ ! -S ~/.ssh/ssh_auth_sock ]; then
  eval `ssh-agent`
  ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
ssh-add -l > /dev/null || {
    ssh-add ~/.ssh/id_rsa && \
    ssh-add ~/.ssh/codeberg && \
    ssh-add ~/.ssh/git
}

