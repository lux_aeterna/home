#!/bin/bash
if [ "$1" = "+" ] ; then
    amixer sset Master 1%+ 1>/dev/null
else
    amixer sset Master 1%- 1>/dev/null
fi
